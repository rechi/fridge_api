
module.exports.routes = {


  /**
   * @api {post} /users Register a new user
   * @apiName RegisterUser
   * @apiGroup Users
   *
   * @apiParam {String} serial  The user's serial ID.
   * @apiParam {String} gcm_key The user's GCM Key.
   * @apiParamExample {json} Request-Example:
   *     HTTP/1.1 200 OK
   *      {
   *        "serial": "123456789",
   *        "gcm_key": "123456789"
   *      }
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      {
   *        "serial": "123456789",
   *        "gcm_key": "123456789",
   *        "createdAt": "2017-01-30T21:50:35.360Z",
   *        "updatedAt": "2017-01-30T21:50:35.360Z",
   *        "id": "588fb52b364d31300f90ebb4"
   *      }
   */
  'post /users': 'UserController.register',

  /**
   * @api {post} /users/:id/fridges Add a Fridge to a User
   * @apiName AddFridge
   * @apiGroup Users
   *
   * @apiParam {String} serial  The user's serial ID.
   * @apiParam {String} label  The association label.
   * @apiParam {String} association_key The fridge association key
   * @apiParamExample {json} Request-Example:
   *     HTTP/1.1 200 OK
   *      {
   *        "association_key": "7238",
   *        "label": "Frigo cuisine"
   *      }
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      {
   *       "owner": 36,
   *       "fridge": 31,
   *       "label": "Frigo cuisine",
   *       "createdAt": "2017-01-31T15:05:01.805Z",
   *       "updatedAt": "2017-01-31T15:05:01.805Z",
   *       "id": 20
   *     }
   *
   */
  'post /users/:serial/fridges': 'UserController.addFridge',

  /**
   * @api {get} /users/:serial/fridges Get user's Fridges
   * @apiName GetUserFridges
   * @apiGroup Users
   *
   * @apiParam {String} serial  The user's serial ID.
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      [
   *       {
   *        "owner": 30,
   *          "fridge": {
   *            "serial": "987654321",
   *            "association_key": "5241062714",
   *            "temp_alert_min": 10,
   *            "temp_alert_max": 10,
   *            "hygro_alert_min": 10,
   *            "hygro_alert_max": 10,
   *            "open_alert_time": 10,
   *            "gas_alert_max": 10,
   *            "createdAt": "2017-01-31T14:59:22.860Z",
   *            "updatedAt": "2017-01-31T14:59:22.860Z",
   *            "id": 25
   *          },
   *          "label": "Frigo cuisine",
   *          "createdAt": "2017-01-31T14:59:27.862Z",
   *          "updatedAt": "2017-01-31T14:59:27.862Z",
   *          "id": 15
   *        }
   *      ]
   *
   */
  'get /users/:serial/fridges': 'UserController.getFridges',

  /**
   * @api {get} /fridges/:serial Find a Fridge by serial
   * @apiName GetFridge
   * @apiGroup Fridges
   *
   * @apiParam {String} serial  The fridge's serial ID.
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "serial": "987654321",
   *       "association_key": "2990306472",
   *       "temp_alert_min": 10,
   *       "temp_alert_max": 10,
   *       "hygro_alert_min": 10,
   *       "hygro_alert_max": 10,
   *       "open_alert_time": 10,
   *       "gas_alert_max": 10,
   *       "createdAt": "2017-01-31T14:07:19.460Z",
   *       "updatedAt": "2017-01-31T14:07:36.109Z",
   *       "id": 6
   *     }
   *
   */
  'get /fridges/:serial': 'FridgeController.find',

  /**
   * @api {post} /fridges Register a new Fridge
   * @apiName RegisterFridge
   * @apiGroup Fridges
   *
   * @apiParam {String} serial  The fridge's serial ID.
   * @apiParamExample {json} Request-Example:
   *     HTTP/1.1 200 OK
   *      {
   *        "serial": "987654321"
   *      }
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      {
   *         "serial": "987654321",
   *         "association_key": "1794893844",
   *         "temp_alert_min": 10,
   *         "temp_alert_max": 10,
   *         "hygro_alert_min": 10,
   *         "hygro_alert_max": 10,
   *         "open_alert_time": 10,
   *         "gas_alert_max": 10,
   *         "createdAt": "2017-01-31T14:02:05.091Z",
   *         "updatedAt": "2017-01-31T14:02:05.091Z",
   *         "id": 2
   *       }
   *
   */
  'post /fridges': 'FridgeController.register',

  /**
   * @api {get} /fridges/:serial/measures Get fridge Measures
   * @apiName FridgeMeasures
   * @apiGroup Measures
   *
   * @apiParam {String} serial  The fridge's serial ID.
   * @apiParam {String} [from] Begin date delimiter (optional).
   * @apiParam {String} [to] End date delimiter (optional).
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *       [
   *         {
   *           "owner": 6,
   *           "temp_1": 10,
   *           "temp_2": 10,
   *           "temp_3": 10,
   *           "hygro": 5,
   *           "gas": 10,
   *           "open": true,
   *           "createdAt": "2017-01-31T14:07:34.895Z",
   *           "updatedAt": "2017-01-31T14:07:34.904Z",
   *           "id": 62
   *         },
   *         {
   *           "owner": 6,
   *           "temp_1": 10,
   *           "temp_2": 10,
   *           "temp_3": 10,
   *           "hygro": 5,
   *           "gas": 10,
   *           "open": true,
   *           "createdAt": "2017-01-31T14:07:36.105Z",
   *           "updatedAt": "2017-01-31T14:07:36.112Z",
   *           "id": 64
   *         }
   *       ]
   */
  'get /fridges/:serial/measures': 'MeasureController.getMeasures',

  /**
   * @api {get} /fridges/:serial/measures/last Get the last fridge Measures
   * @apiName LastFridgeMeasure
   * @apiGroup Measures
   *
   * @apiParam {String} serial  The fridge's serial ID.
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *       [
   *         {
   *           "owner": 6,
   *           "temp_1": 10,
   *           "temp_2": 10,
   *           "temp_3": 10,
   *           "hygro": 5,
   *           "gas": 10,
   *           "open": true,
   *           "createdAt": "2017-01-31T14:07:34.895Z",
   *           "updatedAt": "2017-01-31T14:07:34.904Z",
   *           "id": 62
   *         }
   *       ]
   */
  'get /fridges/:serial/measures/last': 'MeasureController.getLastMeasure',

  /**
   * @api {post} /fridges/:serial/measures Register a new Fridge Measure
   * @apiName NewMeasure
   * @apiGroup Measures
   *
   * @apiParam {String} serial  The fridge's serial ID.
   *
   * @apiSuccess {String} temp_1  The measure's temp_1 value.
   * @apiSuccess {String} temp_2  The measure's temp_2 value.
   * @apiSuccess {String} temp_3  The measure's temp_3 value.
   * @apiSuccess {String} hygro  The measure's hygro value.
   * @apiSuccess {String} gas  The measure's gas value.
   * @apiSuccess {Boolean} open  The measure's open value.
   *
   * @apiParamExample {json} Request-Example:
   *     {
   *       "temp_1": "3.1",
   *       "temp_2": "3.02",
   *       "temp_3": "2.0",
   *       "hygro": "35.1",
   *       "gas": "2.2",
   *       "open": 0
   *     }
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      {
   *         "owner": 40,
   *         "temp_1": 10,
   *         "temp_2": 10,
   *         "temp_3": 10,
   *         "hygro": 5,
   *         "gas": 10,
   *         "open": true,
   *         "createdAt": "2017-01-31T15:49:40.708Z",
   *         "updatedAt": "2017-01-31T15:49:40.708Z",
   *         "id": 429
   *       }
   *
   */
  'post /fridges/:serial/measures': 'MeasureController.createMeasure',

  /**
   * @api {get} /fridges/:serial/events Get fridge Events
   * @apiName FridgeEvents
   * @apiGroup Events
   *
   * @apiParam {String} serial  The fridge's serial ID.
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *     [
   *        {
   *          "owner": 8,
   *          "name": "temp_1",
   *          "value": "12",
   *          "createdAt": "2017-01-31T14:12:56.388Z",
   *          "updatedAt": "2017-01-31T14:12:56.397Z",
   *          "id": 81
   *        },
   *        {
   *          "owner": 8,
   *          "name": "temp_1",
   *          "value": "12",
   *          "createdAt": "2017-01-31T14:12:57.817Z",
   *          "updatedAt": "2017-01-31T14:12:57.825Z",
   *          "id": 83
   *        }
   *      ]
   */
  'get /fridges/:serial/events': 'EventController.getEvents',

  /**
   * @api {post} /fridges/:serial/events Register a new Fridge Event
   * @apiName NewEvent
   * @apiGroup Events
   *
   * @apiParam {String} serial  The fridge's serial ID.
   * @apiParam {String} name  The event's name.
   * @apiParam {String} value  The event's value.
   *
   * @apiParamExample {json} Request-Example:
   *     {
   *       "name": "temp_1",
   *       "value": "35.55"
   *     }
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *      {
   *         "owner": 42,
   *         "name": "temp_1",
   *         "value": "12",
   *         "createdAt": "2017-01-31T15:52:29.575Z",
   *         "updatedAt": "2017-01-31T15:52:29.575Z",
   *         "id": 445
   *       }
   */
  'post /fridges/:serial/events': 'EventController.createEvent',

};
