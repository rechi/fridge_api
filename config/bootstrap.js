/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  User.findOrCreate({"serial": "serialclient", "gcm_key": "gcm_detest"}).exec(function(err, user) {

    Fridge.findOrCreate({serial: "serialdetest", association_key: "1234"}).exec(function(err, fridge) {

      for(var i = 0; i < 20; i++) {
        MeasuresService.createMeasure(fridge.serial, 15.0, 15.0, 15.0, 15.0, 15.0, true, function(err, res) {
          if(err) {
            console.error(err);
          }
        });
        EventsService.createEvent(fridge.serial, "temp_1", "12", function(err, res) {
          if(err) {
            console.error(err);
          }
        });
      }

      UsersService.addFridge(user.serial, fridge.association_key, "mon super frigo", function(err, res) {
        cb();
      });

    });

  });
};
