define({ "api": [
  {
    "type": "get",
    "url": "/fridges/:serial/events",
    "title": "Get fridge Events",
    "name": "FridgeEvents",
    "group": "Events",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n   {\n     \"owner\": 8,\n     \"name\": \"temp_1\",\n     \"value\": \"12\",\n     \"createdAt\": \"2017-01-31T14:12:56.388Z\",\n     \"updatedAt\": \"2017-01-31T14:12:56.397Z\",\n     \"id\": 81\n   },\n   {\n     \"owner\": 8,\n     \"name\": \"temp_1\",\n     \"value\": \"12\",\n     \"createdAt\": \"2017-01-31T14:12:57.817Z\",\n     \"updatedAt\": \"2017-01-31T14:12:57.825Z\",\n     \"id\": 83\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Events"
  },
  {
    "type": "post",
    "url": "/fridges/:serial/events",
    "title": "Register a new Fridge Event",
    "name": "NewEvent",
    "group": "Events",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The event's name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>The event's value.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\": \"temp_1\",\n  \"value\": \"35.55\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n    \"owner\": 42,\n    \"name\": \"temp_1\",\n    \"value\": \"12\",\n    \"createdAt\": \"2017-01-31T15:52:29.575Z\",\n    \"updatedAt\": \"2017-01-31T15:52:29.575Z\",\n    \"id\": 445\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Events"
  },
  {
    "type": "get",
    "url": "/fridges/:serial",
    "title": "Find a Fridge by serial",
    "name": "GetFridge",
    "group": "Fridges",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"serial\": \"987654321\",\n  \"association_key\": \"2990306472\",\n  \"temp_alert_min\": 10,\n  \"temp_alert_max\": 10,\n  \"hygro_alert_min\": 10,\n  \"hygro_alert_max\": 10,\n  \"open_alert_time\": 10,\n  \"gas_alert_max\": 10,\n  \"createdAt\": \"2017-01-31T14:07:19.460Z\",\n  \"updatedAt\": \"2017-01-31T14:07:36.109Z\",\n  \"id\": 6\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Fridges"
  },
  {
    "type": "post",
    "url": "/fridges",
    "title": "Register a new Fridge",
    "name": "RegisterFridge",
    "group": "Fridges",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "HTTP/1.1 200 OK\n {\n   \"serial\": \"987654321\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n    \"serial\": \"987654321\",\n    \"association_key\": \"1794893844\",\n    \"temp_alert_min\": 10,\n    \"temp_alert_max\": 10,\n    \"hygro_alert_min\": 10,\n    \"hygro_alert_max\": 10,\n    \"open_alert_time\": 10,\n    \"gas_alert_max\": 10,\n    \"createdAt\": \"2017-01-31T14:02:05.091Z\",\n    \"updatedAt\": \"2017-01-31T14:02:05.091Z\",\n    \"id\": 2\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Fridges"
  },
  {
    "type": "get",
    "url": "/fridges/:serial/measures",
    "title": "Get fridge Measures",
    "name": "FridgeMeasures",
    "group": "Measures",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "from",
            "description": "<p>Begin date delimiter (optional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "to",
            "description": "<p>End date delimiter (optional).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n    {\n      \"owner\": 6,\n      \"temp_1\": 10,\n      \"temp_2\": 10,\n      \"temp_3\": 10,\n      \"hygro\": 5,\n      \"gas\": 10,\n      \"open\": true,\n      \"createdAt\": \"2017-01-31T14:07:34.895Z\",\n      \"updatedAt\": \"2017-01-31T14:07:34.904Z\",\n      \"id\": 62\n    },\n    {\n      \"owner\": 6,\n      \"temp_1\": 10,\n      \"temp_2\": 10,\n      \"temp_3\": 10,\n      \"hygro\": 5,\n      \"gas\": 10,\n      \"open\": true,\n      \"createdAt\": \"2017-01-31T14:07:36.105Z\",\n      \"updatedAt\": \"2017-01-31T14:07:36.112Z\",\n      \"id\": 64\n    }\n  ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Measures"
  },
  {
    "type": "get",
    "url": "/fridges/:serial/measures/last",
    "title": "Get the last fridge Measures",
    "name": "LastFridgeMeasure",
    "group": "Measures",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n    {\n      \"owner\": 6,\n      \"temp_1\": 10,\n      \"temp_2\": 10,\n      \"temp_3\": 10,\n      \"hygro\": 5,\n      \"gas\": 10,\n      \"open\": true,\n      \"createdAt\": \"2017-01-31T14:07:34.895Z\",\n      \"updatedAt\": \"2017-01-31T14:07:34.904Z\",\n      \"id\": 62\n    }\n  ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Measures"
  },
  {
    "type": "post",
    "url": "/fridges/:serial/measures",
    "title": "Register a new Fridge Measure",
    "name": "NewMeasure",
    "group": "Measures",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The fridge's serial ID.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"temp_1\": \"3.1\",\n  \"temp_2\": \"3.02\",\n  \"temp_3\": \"2.0\",\n  \"hygro\": \"35.1\",\n  \"gas\": \"2.2\",\n  \"open\": 0\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "temp_1",
            "description": "<p>The measure's temp_1 value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "temp_2",
            "description": "<p>The measure's temp_2 value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "temp_3",
            "description": "<p>The measure's temp_3 value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hygro",
            "description": "<p>The measure's hygro value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gas",
            "description": "<p>The measure's gas value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "open",
            "description": "<p>The measure's open value.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n    \"owner\": 40,\n    \"temp_1\": 10,\n    \"temp_2\": 10,\n    \"temp_3\": 10,\n    \"hygro\": 5,\n    \"gas\": 10,\n    \"open\": true,\n    \"createdAt\": \"2017-01-31T15:49:40.708Z\",\n    \"updatedAt\": \"2017-01-31T15:49:40.708Z\",\n    \"id\": 429\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Measures"
  },
  {
    "type": "post",
    "url": "/users/:id/fridges",
    "title": "Add a Fridge to a User",
    "name": "AddFridge",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The user's serial ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>The association label.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "association_key",
            "description": "<p>The fridge association key</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "HTTP/1.1 200 OK\n {\n   \"association_key\": \"7238\",\n   \"label\": \"Frigo cuisine\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n  \"owner\": 36,\n  \"fridge\": 31,\n  \"label\": \"Frigo cuisine\",\n  \"createdAt\": \"2017-01-31T15:05:01.805Z\",\n  \"updatedAt\": \"2017-01-31T15:05:01.805Z\",\n  \"id\": 20\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/:serial/fridges",
    "title": "Get user's Fridges",
    "name": "GetUserFridges",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The user's serial ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n [\n  {\n   \"owner\": 30,\n     \"fridge\": {\n       \"serial\": \"987654321\",\n       \"association_key\": \"5241062714\",\n       \"temp_alert_min\": 10,\n       \"temp_alert_max\": 10,\n       \"hygro_alert_min\": 10,\n       \"hygro_alert_max\": 10,\n       \"open_alert_time\": 10,\n       \"gas_alert_max\": 10,\n       \"createdAt\": \"2017-01-31T14:59:22.860Z\",\n       \"updatedAt\": \"2017-01-31T14:59:22.860Z\",\n       \"id\": 25\n     },\n     \"label\": \"Frigo cuisine\",\n     \"createdAt\": \"2017-01-31T14:59:27.862Z\",\n     \"updatedAt\": \"2017-01-31T14:59:27.862Z\",\n     \"id\": 15\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Register a new user",
    "name": "RegisterUser",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "serial",
            "description": "<p>The user's serial ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gcm_key",
            "description": "<p>The user's GCM Key.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "HTTP/1.1 200 OK\n {\n   \"serial\": \"123456789\",\n   \"gcm_key\": \"123456789\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n   \"serial\": \"123456789\",\n   \"gcm_key\": \"123456789\",\n   \"createdAt\": \"2017-01-30T21:50:35.360Z\",\n   \"updatedAt\": \"2017-01-30T21:50:35.360Z\",\n   \"id\": \"588fb52b364d31300f90ebb4\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./config/routes.js",
    "groupTitle": "Users"
  }
] });
