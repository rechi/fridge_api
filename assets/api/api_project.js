define({
  "name": "Smart Fridge - API",
  "version": "0.0.1",
  "description": "a basic smart fridge API",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-01-31T16:24:54.766Z",
    "url": "http://apidocjs.com",
    "version": "0.17.5"
  }
});
