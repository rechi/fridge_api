# smartfridge-api


## Details 

API Available at: https://smartfridge-api.herokuapp.com/

API Documentation available at: https://smartfridge-api.herokuapp.com/api

Postman collection : [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/45ff2c3d7ccee096fe75)

## Start the API
````shell
# npm install
# npm install -g sails
# sails lift [--prod] [--port xxxx]
or
# node app.js [--prod] [--port xxxx]
````

## Generate the documentation

Type command on shell
````shell
# npm install -g apidoc
# apidoc -e "(node_modules|views|assets|.tmp)" -o assets/api/
````
