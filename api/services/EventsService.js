/**
 * Created by sacha on 30/01/2017.
 */

module.exports = {

  getEvents: function(serial, cb) {
    Fridge.find({serial:serial}).populate('events').exec(function (err, fridges) {
      if(err) {
          cb({error: err, status: 500, message: "Error while getting fridges"});
      }
      else if(fridges.length === 0) {
        cb({error: null, status: 404, message: "Fridge not found"});
      }
      else {
        cb(null, fridges[0].events);
      }
    });
  },

  createEvent: function(serial, name, value, cb) {
    Fridge.find({serial: serial}).populate('events').exec(function(err, fridges) {
      if(err) {
        cb({error: err, status: 500, message: "Error while getting fridge"});
      }
      else if(fridges.length === 0) {
        cb({error: null, status: 404, message: "Fridge not found"});
      }
      else {
          Event.create({
            owner: fridges[0].id,
            name: name,
            value: value
          }).exec(function(err, event_obj) {
            if(err) {
              cb({error: err, status: 500, message: "Error while creating Event"});
            }
            else {
              fridges[0].events.add(event_obj.id);
              fridges[0].save(function(err) {
                if(err) {
                  cb({error: err, status: 500, message: "Error while saving Fridge"});
                } else {
                  cb(null, event_obj);
                }
              });
            }
          });
        }
    });
  }
};
