/**
 * Created by sacha on 30/01/2017.
 */

var async = require('async');

module.exports = {

  register : function(serial, gcm_key, cb) {
    User.find({serial: serial}).exec(function(err, res) {
      if(err) {
        cb({error: err, status: 500, message: "Error while getting users"});
      }
      else if(res.length === 0) {
        User.create({serial:serial, gcm_key:gcm_key}).exec(function(err, res) {
          if(err) {
            cb({error: err, status: 500, message: "Cannot create User"});
          }
          else {
            cb(null, res);
          }
        });
      }
      else {
        cb(null, res[0]);
      }
    });
  },

  addFridge: function(serial, association_key, label, cb) {

    Fridge.find({association_key: association_key}).exec(function(err, fridges) {
      if(err) {
        cb({error: err, status: 500, message: "Error while getting fridge"});
      }
      else if(fridges.length === 0) {
        cb({error: null ,status: 404, message: "Frigo inconnu"});
      }
      else {
        User.find({serial: serial}).exec(function(err, users) {
          if(err) {
            cb({error: err, status: 500, message: "Error while getting user"});
          }
          else if(users.length === 0) {
            cb({error: null, status: 404, message: "User inconnu"});
          }
          else {

            UserHasFridge.find({owner: users[0].id, fridge: fridges[0].id}).exec(function(err, results) {
              if(err) {
                cb({error: err, status: 500, message: "Error while getting userHasFridge"});
              }
              else if(results.length !== 0) {
                cb(null, results[0]);
              }
              else {
                UserHasFridge.findOrCreate({owner: users[0].id, fridge: fridges[0].id, label: label}).exec(function(err, res) {
                  if(err) {
                    cb({error: err, status: 500, message: "Error while saving user has fridge"});
                  }
                  else {
                    cb(null, res);
                  }
                });
              }
            });
          }
        });
      }
    });
  },

  getFridges: function(serial, clb) {
    User.find({serial: serial})
      .populateAll()
      .then(function (users) {
        if (users.length === 0) {
          clb({error: null, status: 404, message: "User not found"});
        }
        else {
          var fridges = users[0].has_fridge;
          async.each(fridges, function(has_fridge, cb) {
            Fridge.find({id: has_fridge.fridge})
              .then(function(res) {
                if(res.length === 0) {}
                else {
                  has_fridge.fridge = res[0];
                  cb();
                }
              })
              .catch(cb);
          }, function(err) {
            if(err) {
              clb({error: err, status: 404, message: "Error"});
            } else {
              clb(null, fridges);
            }
          });
        }
      })
      .catch(function(err) {
        cb({error: err, status: 500, message: "Error while getting users fridge"});
      });

  }

};
