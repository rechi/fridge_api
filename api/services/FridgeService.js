/**
 * Created by sacha on 30/01/2017.
 */

module.exports = {

  register: function(serial, cb) {
    Fridge.find({serial: serial}).exec(function(err, res) {
      if(err) {
        cb({status: 500, message: "Error while getting fridge", error: err});
      }
      else if(res.length === 0) {
        var association_key = Math.floor((Math.random() * 10000000000) + 1) + "";
        Fridge.create({serial: serial, association_key: association_key})
          .exec(function(errCreating, res) {
          if(errCreating) {
            cb({status: 500, message: "Error while creating fridge", error: errCreating});
          }
          else {
            cb(null, res);
          }
        });
      }
      else {
        console.log("[SER] Fridge aready registered");
        cb(null, res[0]);
      }
    });
  },

  find: function(serial, cb) {
    Fridge.find({serial:serial}).exec(function(err, res) {
      if(err) {
        cb({status: 500, message: "Error while getting fridges", error: err});
      }
      else {
        if(res.length) {
          cb(null, res);
        }
      }
    });
  }
};
