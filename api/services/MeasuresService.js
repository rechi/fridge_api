/**
 * Created by sacha on 30/01/2017.
 */

var moment = require('moment');

module.exports = {

  getLastMeasure: function(serial, cb) {
    Fridge.find({serial:serial}).exec(function (err, fridges) {
      if(err) {
        cb({error: err, status: 500, message: "Error getting fridge"});
      }
      else if(fridges.length === 0) {
        cb({error: null, status: 404, message: "Fridge not found"});
      }
      else {
        Measure.find({where: {owner: fridges[0].id}, sort: 'createdAt DESC', limit: 1}).exec(function(error, results) {
          if(error) {
            cb({error: error, status: 500, message: "Error getting fridge"});
          }
          else if(results.length === 0) {
            cb({error: null, status: 404, message: "Measure not found"});
          }
          else {
            cb(null, results[0]);
          }
        });
      }
    });
  },

  getMeasures: function(serial, from, to, cb) {
    Fridge.find({serial: serial}).populate('measures').exec(function (err, fridges) {
      if(err) {
        cb({error: err, status: 500, message: "Error getting fridge"});
      }
      else if(fridges.length === 0) {
        cb({error: null, status: 404, message: "Fridge not found"});
      }
      else {
        var criteria =  {owner: fridges[0].id};
        if(from || to)  {criteria.createdAt = {}}
        if(to)          {criteria.createdAt.$lt = new Date(to);}
        if(from)        {criteria.createdAt.$gte = new Date(from);}

        Measure.find({where: criteria, sort: 'createdAt DESC'}).exec(function(error, results) {
          if(error) {
            cb({error: error, status: 500, message: "Error getting fridge"});
          }
          else {
            cb(null, results);
          }
        });
      }
    });
  },

  createMeasure: function(serial, temp_1, temp_2, temp_3, hygro, gas, open, cb) {

    Fridge.find({serial: serial}).populate('measures').exec(function(err, fridges) {
      if(err) {
        cb({error: err, status: 500, message: "Error getting fridge"});
      }
      else if(fridges.length === 0) {
        cb({error: null, status: 404, message: "Fridge not found"});
      }
      else {
        Measure.create({owner: fridges[0].id, temp_1: temp_1, temp_2: temp_2, temp_3: temp_3, hygro: hygro, gas: gas, open: open})
          .exec(function(err, measure_obj) {
          if(err) {
            cb({error: err, status: 500, message: "Error while creating measure"});
          }
          else {
            fridges[0].measures.add(measure_obj.id);
            fridges[0].save(function(err) {
              if(err) {
                cb({error: err, status: 404, message: "Error while saving fridge"});
              } else {
                cb(null, measure_obj);
              }
            })
          }
        });
      }
    });
  }
};
