/**
 * Measure.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    owner: {
      model: 'Fridge'
    },

    temp_1: {
      type: 'float',
      required: true
    },

    temp_2: {
      type: 'float',
      required: true
    },

    temp_3: {
      type: 'float',
      required: true
    },

    hygro: {
      type: 'float',
      required: true
    },

    gas: {
      type: 'float',
      required: true
    },

    open: {
      type: 'boolean',
      required: true
    }
  }
};

