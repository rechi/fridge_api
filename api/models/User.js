/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    serial: {
      type: 'string',
      required: true
    },

    gcm_key: {
      type: 'string',
      required: true
    },

    has_fridge: {
      collection: 'UserHasFridge',
      via: 'owner'
    }
  }
};

