/**
 * Fridge.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    serial: {
      type: 'string',
      required: true
    },

    association_key: {
      type: 'string',
      required: true
    },

    temp_alert_min: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },
    temp_alert_max: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },
    hygro_alert_min: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },
    hygro_alert_max: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },
    open_alert_time: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },
    gas_alert_max: {
      type: 'float',
      required: true,
      defaultsTo: 10
    },


    measures: {
      collection: 'Measure',
      via: 'owner'
    },

    events: {
      collection: 'Event',
      via: 'owner'
    },

    has_users: {
      collection: 'UserHasFridge',
      via: 'fridge'
    }
  }
};

