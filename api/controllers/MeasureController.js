/**
 * MeasureController
 *
 * @description :: Server-side logic for managing measures
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  getLastMeasure: function(req, res) {
    console.log('[API] Getting measures for Fridge ', req.param('serial'));
    MeasuresService.getLastMeasure(req.param('serial'), function(err, results) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.json(results);
      }
    })
  },

  getMeasures: function(req, res) {
    console.log('[API] Getting measures for Fridge ', req.param('serial'));
    MeasuresService.getMeasures(req.param('serial'), req.param('from'), req.param('to'), function(err, results) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.json(results);
      }
    })
  },

  createMeasure: function(req, res) {
    console.log('[API] Adding Meausure to Fridge', req.param('serial'));
    var temp_1 = req.param('temp_1'),
      temp_2 = req.param('temp_2'),
      temp_3 = req.param('temp_3'),
      gas = req.param('gas'),
      hygro = req.param('hygro'),
      open = req.param('open'),
      from = req.param('from'),
      to = req.param('to');

    MeasuresService.createMeasure(req.param('serial'), temp_1, temp_2, temp_3, hygro, gas, open, function(err, result) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.send(result);
      }
    })
  },
};

