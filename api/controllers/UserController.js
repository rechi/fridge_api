/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  register: function(req, res) {
    console.log('[API] Registering new User', req.param('serial'),  req.param('gcm_key'));
    UsersService.register(
      req.param('serial'),
      req.param('gcm_key'), function (err, result) {
        if(err) {
          console.error("Error", err);
          res.status(err.status);
          res.end(err.message);
        }
        else {
          res.send(result);
        }
      });
  },

  addFridge: function(req, res) {
    console.log('[API] Adding fridge to User', req.param('serial'), "with label", req.param('label'));

    if(req.param('serial') && req.param('association_key')) {
      UsersService.addFridge(req.param('serial'), req.param('association_key'), req.param('label'), function(err, result) {
        if(err) {
          console.error("Error", err);
          res.status(err.status);
          res.end(err.message);
        }
        else {
          res.json(result);
        }
      });
    }
    else {
      res.status(400);
    }
  },


  getFridges: function(req, res) {
    console.log('[API] Getting fridges of User', req.param('serial'));
    UsersService.getFridges(req.param('serial'), function(err, results) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.json(results);
      }
    });
  }

};

