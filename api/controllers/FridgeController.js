/**
 * FridgeController
 *
 * @description :: Server-side logic for managing fridges
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  register: function(req, res) {
    console.log('[API] Registering new Fridge', req.param('serial'));
    FridgeService.register(req.param('serial'),
      function (err, result) {
        if(err) {
          console.error(err);
          res.status(err.status);
          res.end(err.message);
        }
        else {
          res.send(result);
        }
      });
  },

  find: function(req, res) {
    console.log('[API] Getting fridge for serial', req.param('serial'));
    FridgeService.find(req.param('serial'), function(err, result) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else if(result.length === 0) {
        res.send(404);
      } else {
        res.json(result[0]);
      }
    })
  }
};

