/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  getEvents: function(req, res) {
    console.log('[API] Getting Events for Fridge ', req.param('serial'));
    EventsService.getEvents(req.param('serial'), function(err, results) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.json(results);
      }
    })
  },

  createEvent: function(req, res) {
    console.log('[API] Adding Event to Fridge', req.param('serial'));
    EventsService.createEvent(req.param('serial'), req.param('name'), req.param('value'), function(err, result) {
      if(err) {
        console.error(err);
        res.status(err.status);
        res.end(err.message);
      }
      else {
        res.send(result);
      }
    });
  },
};

